# Solution for Foobarnian Elections calculation

## Notes

> In reality, the Republic of Foobarnia has a
> huge population - **9007199254740992** to be exact - and *everyone* votes.

Actually, this means, that in JS full array of votes can not exist, because
`Number.MAX_SAFE_INTEGER + 1` is more than Array size limit (actually, it is `Number.MAX_SAFE_INTEGER`).

So I've added support of votes sequences to `tally` function. If you need to check more than
`Number.MAX_SAFE_INTEGER` votes - you should split it in parts and pass them as votes. I've added tests
for this mode accordingly.

By the way this does not cover case when every citizien of Foobarnia votes for himself. In this case
we should reimplement function to use external data storage and use parallelization of calculations.
Personally I prefer in this case implement in other languages than JS. For this particular case
Hadoop claster will fit good.

## Requirements notes

For me it looks like that requirements miss several edge cases. E.g. Foobarnia had national disaster recently
and noone took part in elections. I've added test for this case. No candidates - no winners. But we should write down
updated requirements.

> 3. Also in the runoff case, if there are multiple 3rd place candidates in a tie,
> then all candidates who tied for third place will qualify for the runoff. So in
> that situation, an Array with more than three integers may need to be returned.

Another caveat is rule #3, because we only take care of multiple 3rd place candidates. What if we have tie
between candidates for 1st or 2nd place?

Actually, test named `election runoff with tie` breaks rule #3 - because it takes results with 4 and 3 votes
accordingly, which actually are 1st place candidate and multiple 2nd place candidates.

But I've implemented `tally` function to satisfy tests - maybe another developer already got updated requirements.
We should definitely discuss this additionally.

## Implementation notes

I've tried to do fast return in every place I can. By the way if we want to optimize it more - we should
replace `Set` with `Object` - `Object` with `.property = true` is a bit faster than `Set`.

I left some comments in source code to explain complexity and fast-off branches of code.
