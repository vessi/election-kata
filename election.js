function calculateMaxValues(candidates, unique) {
  const maxValues = new Set();
  let sorted = Object.values(candidates).sort((a,b) => (b - a))
  if (!unique) {
    sorted = sorted.slice(0, 3);
  }
  for (let winner of sorted) {
    maxValues.add(winner);
    if (maxValues.size >= 3) {
      break;
    }
  }
  return maxValues;
}

function calculateWinners(candidates, maxValues) {
  const winners = [];
  for (let candidate in candidates) {
    if (maxValues.has(candidates[candidate])) {
      winners.push(parseInt(candidate));
    }
  }
  return winners;
}

function sumPortions(candidates) {
  return candidates.reduce((accum, portion) => {
    for (let candidate in portion) {
      accum[candidate] = (accum[candidate] || 0) + portion[candidate];
    }
    return accum;
  }, {});
}

function calculateSum(votes, majority) {
  const voteSum = {}
  for (let vote of votes) {
    voteSum[vote] = 1 + (voteSum[vote] || 0);
    // if someone reach majority of votes - break immediately
    if (majority && voteSum[vote] >= majority) {
      voteSum.winner = [vote];
      break;
    }
  }
  return voteSum;
}

exports.tally = function(votes) {
  let majority = 0;
  let candidates;
  const sequence = votes.some((el) => Array.isArray(el));
  if (sequence) {
    /*
      if we are working with votes by parts (supports more than MAXINT arrays by splitting),
      we are losing fast-lane of majority winner check (rule #1), it's price of larger data
      support. Complexity is always O(n)
    */
    const commonLength = votes.reduce((sum, votePortion) => sum + votePortion.length, 0);
    majority = Math.round( commonLength / 2 );
    const portionResults = votes.map((votePortion) => calculateSum(votePortion, majority));
    // Maybe we have winner just in one portion
    const majorityReached = portionResults.find((result) => result.winner);
    if (majorityReached) {
      return majorityReached;
    }
    // No? Calculate common results
    candidates = sumPortions(portionResults);
    // Check them for majority
    for (candidate in candidates) {
      if (candidates[candidate] >= majority) return [parseInt(candidate)];
    }
  } else {
    /*
      let's count votes. complexity from O(n/2) (majority reached fast)
      to O(n) - majority not reached or reached at last element
      n == number of votes
    */
    majority = Math.round( votes.length / 2 );
    candidates = calculateSum(votes, majority);
    if (candidates.winner) {
      return candidates.winner;
    }
  }

  const maxValues = calculateMaxValues(candidates);
  const winners = calculateWinners(candidates, maxValues);
  return winners;
}
